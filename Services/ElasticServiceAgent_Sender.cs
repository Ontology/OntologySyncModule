﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologySyncModule.Notifications;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OntologySyncModule.Services
{
    public class ElasticServiceAgent_Sender : NotifyPropertyChange
    {
        public clsLocalConfig localConfig;

        private OntologyModDBConnector _dbLevelJob;
        private OntologyModDBConnector _dbLevelActive;
        private OntologyModDBConnector _dbLevelWebservice;
        private OntologyModDBConnector _dbLevelWebserviceRel;
        private OntologyModDBConnector _dbLevelUserAuthRel;
        private OntologyModDBConnector _dbLevelAllOntologies;
        private OntologyModDBConnector _dbLevelOntologiesOfBaseData;
        private OntologyModDBConnector _dbLevelDirection;

        private Thread getDataAsync;

        public clsOntologyItem OItem_Configuration { get; set; }

        private List<clsObjectRel> configurationToJobsList;
        public List<clsObjectRel> ConfigurationToJobsList
        {
            get
            {
                return configurationToJobsList;
            }
            set
            {
                configurationToJobsList = value;
                RaisePropertyChanged(NotifyChanges.ESServiceAgent_ConfigurationToJobsList);
            }
        }

        public ElasticServiceAgent_Sender(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }

        private void Initialize()
        {

        }


        public clsOntologyItem LoadData(clsOntologyItem oItemConfiguration)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            OItem_Configuration = oItemConfiguration;

            return result;

        }
    }
}
